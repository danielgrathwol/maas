/* eslint-disable prefer-const */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


export default new Vuex.Store({
  state: {

    storyparts: ["Home", "SnapHome", "SbahnHome", "PaymentHome",
      "transitionairport", "transitioncentralstation", "AirportStoryOne",
      "SnapStoryTwo",
      "PayRideSnapThree",
      "TrainStoryOne", "SbahnStoryTwo",
      "PayRideSbahnThree"
    ],
    menu_toggle: false,
    currentStoryPart: 0,
    currentStoryPageCount: 0,
    currentInnerPageCount: [0, 0],
    pagecountChangeTrigger: 0,
    storypath: "snap",
    videoatfirst: true,
    endstate: false,
    showSolutionPoints: false,
    showLobbySAP: true,
    modals: {
      SmcEYIntroduction: {
        show: true,
        load: true,
      },
      SmcEYVision: {
        show: false,
        load: true,
      },
      SmcEYMobUseCase: {
        show: false,
        load: true,
      },
      SmcEYMobBusinessTrans: {
        show: false,
        load: true,
      },
      SmcEYOrganicTrans: {
        show: false,
        load: true,
      },
      SmcEYNonOrganicTrans: {
        show: false,
        load: true,
      },
      SmcEYTransBackoffice: {
        show: false,
        load: true,
      },
      SmcEYITTrans: {
        show: false,
        load: true,
      },
      SmcEYCybersec: {
        show: false,
        load: true,
      },
    },
    frameworktypes: ['partpicture', 'fullpicture', 'fullvideo', 'iframe'],
    storyframeworkconfig: [{ //AirportStoryOne
        iframe: false,
        slider: true,
        picture: true,
        video: true
      },
      { //TrainStationStoryOne
        iframe: false,
        slider: true,
        picture: true,
        video: true
      },
      { //SbahnStoryTwo
        iframe: false,
        slider: true,
        picture: true,
        video: true
      },
      { //SnapStoryTwo
        iframe: true,
        slider: true,
        picture: true,
        video: true
      },
      { //PayRideThree
        iframe: false,
        slider: true,
        picture: true,
        video: false
      }
    ]
  },
  mutations: {
    incrementPageCount(state) {
      state.currentStoryPageCount++;
    },
    decrementPageCount(state) {
      if (state.currentStoryPageCount != 0) {
        state.currentStoryPageCount--;
      }
    },
    changeVideoFirst(state) {
      state.videoatfirst = false;
    },
    initalVideoFirst(state) {
      state.videoatfirst = true;
    },
    changeStandardLobby(state) {
      state.showLobbySAP = !state.showLobbySAP;
    },
    incrementInnerPageCount(state, row) {
      if (row == state.currentInnerPageCount.length) {
        state.currentInnerPageCount[row] = state.currentInnerPageCount[row] + 1;
        state.pagecountChangeTrigger++;
      } else {
        state.currentInnerPageCount[row] = state.currentInnerPageCount[row] + 1;
        state.pagecountChangeTrigger++;
      }

    },
    decrementInnerPageCount(state) {
      state.currentInnerPageCount--;
    },
    setEndstate(state) {
      state.endstate = true;
    },
    setInitialPageCount(state) {
      state.currentStoryPageCount = 0;
      state.currentInnerPageCount = [0, 0];
      state.pagecountChangeTrigger = 0;
      state.endstate = false;
    },
    setInitialCurrentInnerPageCount(state) {
      state.currentInnerPageCount = [0, 0];
      state.pagecountChangeTrigger = 0;
    },
    toggleMenu(state) {
      state.menu_toggle = !state.menu_toggle;
    },
    setStoryPath(state, path) {
      state.storypath = path;
    },
    setStoryPart(state, name) {
      let index = state.storyparts.indexOf(name);
      if (index) {
        state.currentStoryPart = index;
      }

    }
  },
  actions: {

  },
  getters: {
    selectedStoryPart(state) {
      return state.storyparts[state.currentStoryPart];
    },
    getNextStoryPart(state) {
      return state.storyparts[state.currentStoryPart + 1];
    }

  }
});
