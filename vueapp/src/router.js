import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import StoryDialerView from './views/StoryDialerView.vue';
import AirportStoryOne from './views/AiportStoryOne.vue';
import TrainStationStoryOne from './views/TrainStationStoryOne.vue';
import SbahnStoryTwo from './views/SbahnStoryTwo.vue';
import SnapStoryTwo from './views/SnapStoryTwo.vue';
import PayRideSbahnThree from './views/PayRideSbahnThree.vue';
import PayRideSnapThree from './views/PayRideSnapThree.vue';
import SolutionMap from './views/SolutionMap.vue';
import TransitionAirport from './views/TransitionAirport.vue';
import Survey from './views/Survey.vue';
import HomePod2 from './views/HomePod2.vue';
import HomePod2Bahn from './views/HomePod2Bahn.vue';
import TransitionCentralStation from './views/TransitionCentralStation.vue';
import HomePod3 from './views/HomePod3.vue';
import EYWrapper from './views/EYWrapper.vue';
import SolutionMapTest from './views/SolutionMapTest.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/home2snap',
      name: 'SnapHome',
      component: HomePod2,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/home2bahn',
      name: 'SbahnHome',
      component: HomePod2Bahn,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/homepay',
      name: 'PaymentHome',
      component: HomePod3,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/transair',
      name: 'transitionairport',
      component: TransitionAirport,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/transcentralstation',
      name: 'transitioncentralstation',
      component: TransitionCentralStation,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/story',
      name: 'storydialerview',
      component: StoryDialerView,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/airport',
      name: 'AirportStoryOne',
      component: AirportStoryOne,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/trainstation',
      name: 'TrainStoryOne',
      component: TrainStationStoryOne,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/sbahn',
      name: 'SbahnStoryTwo',
      component: SbahnStoryTwo,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/snap',
      name: 'SnapStoryTwo',
      component: SnapStoryTwo,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/paysbahn',
      name: 'PayRideSbahnThree',
      component: PayRideSbahnThree,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/paysnap',
      name: 'PayRideSnapThree',
      component: PayRideSnapThree,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/lobby/:path',
      props: true,
      name: 'lobby',
      //component: SolutionMapTest,
      component: SolutionMap,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/survey',
      name: 'Surveyscreen',
      component: Survey,
    },
    {
      path: '/ey/:point',
      props: true,
      name: 'ey',
      component: EYWrapper,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import( /* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
});
