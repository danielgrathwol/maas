'use strict';

const express = require('express');
const app = express();
const http = require('http').Server(app);
const WebSocket = require('ws');
const cfenv = require('cfenv');
const hdb = require('./db/hdbconnect');
const bodyParser = require('body-parser');
const path = require('path');

let port = process.env.PORT || 8080;
let appEnv = cfenv.getAppEnv();
let hdbPool;
let clientList = new Array();
let raspberryConnected = false;


var insertOBDData = (data, hdbPool) => {

  if (hdbPool) {
    var perpareSQL = 'INSERT INTO \"TRANSINSIGHT_1\".\"transportationinsights.data::OBDIIDATA.FastData\" (\"TIMESTAMP\", \"SPEED\", \"RPM\", \"ENGINELOAD\", \"COOLANTTEMP\", \"THROTTLEPOS\", \"INTAKETEMP\", \"ENGINERUNTIME\", \"FUELRATE\")' +
      ' values(?,?,?,?,?,?,?,?,?)';
    var prepArgs = [data[0].timestamp, data[1].speed, data[2].rpm, data[3].engineload, data[4].coolant, data[5].throttle, data[6].intaketemp, data[7].engineruntime, data[8].fuelrate];
    console.log(perpareSQL);
    hdbPool.exec(perpareSQL, prepArgs, function (err) {
      if (err) {
        console.log('Database error' + err);
      }
    });
  } else {
    console.log('No pool is available!');
  }

}

var insertOBDDataSlow = (data, hdbPool) => {

  if (hdbPool) {
    //var tempFuel = data[7].fuelstatus
    var perpareSQL = 'INSERT INTO \"TRANSINSIGHT_1\".\"transportationinsights.data::OBDIIDATA.SlowData\" (\"TIMESTAMP\", \"STATUSMIL\", \"STATUSDTCCOUNT\", \"STATUSIGNITIONTYPE\", \"OILTEMP\", \"DISTANCESINCEDTC\", \"BATTERYLIFE\")' +
      ' values(?,?,?,?,?,?,?)';
    var prepArgs = [data[0].timestamp, data[1].statusMIL, data[2].statusDTC, data[3].statusIgnition, data[4].oiltemp, data[5].dtcclear, data[6].batteryrem];
    console.log(perpareSQL);
    hdbPool.exec(perpareSQL, prepArgs, function (err) {
      if (err) {
        console.log('Database error' + err);
      }
    });
  } else {
    console.log('No pool is available!');
  }

}

var insertOBDDataDTC = (data, hdbPool) => {

  if (hdbPool) {
    //GET DTC Codes and check if empty
    var tempDTC = data[7].dtc
    if (tempDTC != []) {
      tempDTC.forEach ((obj) => {
        var perpareSQL = 'INSERT INTO \"TRANSINSIGHT_1\".\"transportationinsights.data::OBDIIDATA.DTC\" (\"TIMESTAMP\", \"TROUBLECODE\", \"TCEXPLANATION\")' +
          ' values(?,?,?)';
        var prepArgs = [data[0].timestamp, obj.dtccode, obj.dtctext];
        console.log(perpareSQL);
        console.log(prepArgs);
        hdbPool.exec(perpareSQL, prepArgs, function (err) {
          if (err) {
            console.log('Database error' + err);
          }
        });

      });
    }
  } else {
    console.log('No pool is available!');
  }


}

app.use(express.static(__dirname + '/client/web'))
// requests for /static/js/index.js will return public/js/index.js
require('./client/routes/routes')(app);


http.listen(port, () => {
  console.log("Server listening on url: " + url + "port: " + port);

});


const wss = new WebSocket.Server({
  server: http
});

wss.on('connection', (ws) => {
  console.log('Inside connection');
  //connection is up, let's add a simple simple event
  ws.on('message', (message) => {
    console.log('Message received' + message)
    var marshalledData = JSON.parse(message)
    var key = Object.keys(marshalledData)[0]

    if (key == 'register') {
      try {
        var clientObj = {};
        clientObj.ip = ws._socket.remoteAddress;
        clientObj.name = marshalledData.register;
        clientObj.id = ws.id;
        clientObj.ws = ws;
        clientList.push(clientObj);
        if (marshalledData.register == 'raspberry') {
          raspberryConnected = true;
        } else {
          wss.clients.forEach(function each(client) {
            if (client !== ws && client.readyState === WebSocket.OPEN) {
              client.send(JSON.stringify({
                'raspberry': raspberryConnected.toString()
              }));
            }
          });
        }
      } catch (error) {
        console.log('Error inside register ' + error);
      }
    } else if (key == 'fastdata') {
      insertOBDData(marshalledData.fastdata, hdbPool)
      for (let i = 0; i < clientList.length; i++) {
        if (clientList[i].name == 'browser') {
          clientList[i].ws.send(JSON.stringify(marshalledData.fastdata));
        }
      }

    } else if (key == 'slowdata') {
      insertOBDDataSlow(marshalledData.slowdata, hdbPool)
      insertOBDDataDTC(marshalledData.slowdata, hdbPool)
      for (let i = 0; i < clientList.length; i++) {
        if (clientList[i].name == 'browser') {
          clientList[i].ws.send(JSON.stringify(marshalledData.slowdata));
        }
      }
    }

  });
  ws.on('close', function (connection) {
    // close user connection
    console.log('Connection closed');
    for (let i = 0; i < clientList.length; i++) {
      if (clientList[i].id == ws.id) {
        if (clientList[i].name == 'raspberry') {
          raspberryConnected = false;
        }
        clientList.splice(i, 1);

      }
    }
  });
  //send immediatly a feedback to the incoming connection    
  ws.send('Server connected');
});


if (process.env.VCAP_APPLICATION) {
  //app is running in the cloud
  var application = JSON.parse(process.env.VCAP_APPLICATION);
  var uris = application['uris'][0];
  var url = "http://" + uris;
  try {
    console.log('Set env variables')
    hdbPool = hdb.setEnvVariables();
  } catch (error) {
    console.log("Error Message: " + error);
  }
}

module.exports.hdbPool = hdbPool