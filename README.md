# Installation Instructions
## Prerequisites
1. Install NodeJS: [NodeJS Windows](https://nodejs.org/dist/v10.16.3/node-v10.16.3-x64.msi)
2. Install Git-Client: [Git-Client Windows](https://git-scm.com/download/win)
## Clone repository and serve application
- Go to the folder you want to save the project.
- Open terminal
- Enter in terminal: `cd` *your folder path* -> Hit Enter
- Enter in terminal: `git clone https://gitlab.com/danielgrathwol/maas.git` -> Hit Enter
- Enter in terminal: `cd maasiaa/vueapp`
- Enter in terminal: `npm install serve`
- Enter in terminal: `npx serve -s dist`
- Open [localhost](http://localhost:5000) in Google Chrome
## Online Version
[SAP Cloud Platform MaaS Story](http://maasiaa-app.cfapps.eu10.hana.ondemand.com)
